# Changelog

All notable changes to this project will be documented in this file.

## [1.0.0]

Initial release


---

## [2.0.0] - 2025-03-03
### Added
- Added an action to fetch NFT given the MNKR UUID.

### Changed
- 🚨 **Breaking Change:** Updated `NftServiceTrait` method names:
  - `uploadNft` → `uploadNMKRNft`
  - `deleteNft` → `deleteNMKRNft`
  - `updateNft` → `updateNMKRNft`

### Migration Guide
- Update all references to these renamed methods in your codebase.