# Nft Maker

[![Latest Version on Packagist](https://img.shields.io/packagist/v/lido-nation/cardano-nft-maker-laravel.svg?style=flat-square)](https://packagist.org/packages/lido-nation/cardano-nft-maker)
![Packagist Downloads](https://img.shields.io/packagist/dt/lido-nation/cardano-nft-maker-laravel)
![GitLab Release](https://img.shields.io/gitlab/v/release/lidonation%2Fcardano-nft-maker-laravel?gitlab_url=https%3A%2F%2Fgitlab.lidonation.com)
![GitLab Tag](https://img.shields.io/gitlab/v/tag/lidonation%2Fcardano-nft-maker-laravel?gitlab_url=https%3A%2F%2Fgitlab.lidonation.com%2F)

This package aims at making the cardano nft creation easy, We use [NMKR Studio](https://studio.nmkr.io/) behind the scenes to make this process seemless.

## Support us

[<img src="https://www.lidonation.com/img/llogo-transparent.png" width="419px" />](https://www.lidonation.com/)

You can support us by [buying one of our nfts or donating](https://www.lidonation.com/en/bazaar).

## Installation

You can install the package via composer:

```bash
composer require lidonation/cardano-nft-maker-laravel
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="cardano-nft-maker-laravel-config"
```

This is the contents of the published config file:

```php
return [
    'auth_key' => env('MAKER_AUTH_KEY', ''),
    'baseUrl' => env('MAKER_BASE_URL', 'https://studio-api.preprod.nmkr.io'),
    'project_uuid' => env('MAKER_PROJECT_UUID',)
];
```

## Usage

**Set up a project** at [NMKR Studio](https://studio.nmkr.io/).

To use this package your dedicated nft model neets to implement the `CardanoNftInterface` that has predefined properties `maker_nft_uuid` which will be the uuid of the remote nft that will be created in the api and `maker_project_uuid` the uuid of the project your remote nft will be under.For simplicity we will make the properties as columns in our nft model.
Use `NftServiceTrait` which  lets you utilise the `NftMakerService` to make the api calls.

```php
<?php

namespace App\Models;

use Lidonation\CardanoNftMaker\Interfaces\CardanoNftInterface;
use Lidonation\CardanoNftMaker\Traits\NftServiceTrait;

class Nft extends Model implements CardanoNftInterface
{
    use NftServiceTrait;
}

```

lets upload our nft to the api

```php
//get our nft instance 
$myNft = Nft::first();

$metadata = [
    "tokenname" => $myNft->name,
    "displayname" => $myNft->name,
    "description" => $myNft->description,
    "previewImageNft" => [
        "mimetype" => "image/png",
        "fileFromsUrl" => "https://images.pexels.com/photos/45201/kitty-cat-kitten-pet-45201.jpeg?auto=compress&cs=tinysrgb&w=800",
    ],
    "metadataPlaceholder" => [
        [
            "name" => "Age",
            "value" => "1"
        ],
        [
            "name" => "Color",
            "value" => "Black and white"
        ]
    ],
];

//our metadata has to conform to the format specified by the api, our DTO can do the validation for us 
$validatedMetadata = MetadataUpload::from($metadata);

//set the remote project uuid
$myNft->update([
  'maker_project_uuid' => '92fa1da3-df83-40b9-a21b-dc819553e98b'
]);

//upload
/* @var $response \Saloon\Http\Response  */
$response = $mynft->uploadNMKRNft($validatedMetadata);

//response details 
$remoteNftDetails = $response->json();

//save the nftUid for further updating if required, we will also save our metadata for updating later 
$metadataDetails = $remoteNftDetails['metadata'];
$myNft->update([
  'maker_nft_uuid' => $remoteNftDetails['nftUid'],
  'maker_nft_metadata' => $metadataDetails,
  'policy' => array_key_first(json_decode($metadataDetails, true)[721])
]);
```

If we need to update our metadata, we can easily do that. For our case we will add social contacts.

```php
$metadataDetails = json_decode($myNft->maker_nft_metadata, true);
$metadata = $metadataDetails[721][$myNft->policy][$myNft->name];

//remove empty fields if any
$updatedMetadata = array_filter($metadata, fn ($value) => !empty($value));

// add custom static metadata
$updatedMetadata['web contact'] = [
    'twitter' => 'https://x.com/catAstrophy',
    'discord' => 'https://discordapp.com/users/7734909132320',
    'website' => 'https://www.cat-astrophy.com',
];
$metadataDetails[721][$myNft->policy][$myNft->name] = $updatedMetadata;

//lets update remote our nft
$response = $myNft->updateNMKRNft($metadataDetails)->json();

//sync local with remote nft
$myNft->update([
  'maker_nft_metadata' => $response['metadata'],
]);

```

To delete our remote nft

```php
$myNft->deleteNMKRNft();
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Credits

- [lidonation.com](https://github.com/Lidonation)
- [Emmanuel Titi](https://github.com/Emmanuel-SE)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
