<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Integrations;

use Saloon\Http\Connector;
use Saloon\Traits\Plugins\AcceptsJson;

class NftMakerConnector extends Connector
{
    use AcceptsJson;

    public ?int $tries = 3;

    public ?int $retryInterval = 100;

    /**
     * The Base URL of the API
     */
    public function resolveBaseUrl(): string
    {
        return config('cardano-nft-maker.baseUrl');
    }

    /**
     * Default headers for every request
     *
     * @return string[]
     */
    protected function defaultHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json, text/plain',
            'Authorization' => 'Bearer '.config('cardano-nft-maker.auth_key'),
        ];
    }

    /**
     * Default HTTP client options
     *
     * @return string[]
     */
    protected function defaultConfig(): array
    {
        return [
            'timeout' => 300,
        ];
    }
}
