<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Integrations\Requests;

use Saloon\Enums\Method;
use Saloon\Http\Request;
use Saloon\Http\Connector;
use InvalidArgumentException;
use Saloon\Contracts\Body\HasBody;
use Saloon\Traits\Body\HasJsonBody;
use Saloon\Traits\Request\HasConnector;
use Lidonation\CardanoNftMaker\Integrations\NftMakerConnector;

class NftMakerRequest extends Request implements HasBody
{
    use HasConnector, HasJsonBody;

    /**
     * Define the HTTP method
     */
    protected Method $method;

    protected ?string $endpoint;

    public function __construct(?string $method)
    {
        $this->body()->setJsonFlags(JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_THROW_ON_ERROR);

        if ($method) {
            $this->method = $method;
        }
    }

    /**
     * Sets the HTTP method for the request.
     *
     * @param Method $method The HTTP method to be used for the request.
     *
     * @return void
     *
     * @throws InvalidArgumentException If the provided method is not a valid HTTP method.
     */
    public function setMethod(Method $method): void
    {
        $this->method = $method;
    }

    /**
     * Resolves the connector for the request.
     *
     * @return Connector The connector instance to be used for the request.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException If the connector cannot be resolved from the container.
     */
    public function resolveConnector(): Connector
    {
        return app(NftMakerConnector::class);
    }

    /**
     * Define the endpoint for the request
     */
    public function resolveEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * Sets the endpoint for the request.
     *
     * @param string $endpoint The endpoint to be used for the request.
     *
     * @return void
     *
     * @throws InvalidArgumentException If the provided endpoint is not a valid string.
     */
    public function setEndPoint($endpoint)
    {
        if (!is_string($endpoint)) {
            throw new InvalidArgumentException('Endpoint must be a string.');
        }

        $this->endpoint = $endpoint;
    }
}
