<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Services;

use Throwable;
use Saloon\Enums\Method;
use Saloon\Http\Response;
use Illuminate\Support\Facades\Log;
use Lidonation\CardanoNftMaker\DTO\MetadataUpload;
use Lidonation\CardanoNftMaker\Exceptions\ApiCallException;
use Lidonation\CardanoNftMaker\Interfaces\CardanoNftInterface;
use Lidonation\CardanoNftMaker\Exceptions\TypeMismatchException;
use Lidonation\CardanoNftMaker\Exceptions\NftAlreadyExistException;
use Lidonation\CardanoNftMaker\Exceptions\AttributeNotFoundException;
use Lidonation\CardanoNftMaker\Integrations\Requests\NftMakerRequest;

class NftMakerService
{
    /**
     * Uploads a new NFT to the Cardano NFT Maker API.
     *
     * @param CardanoNftInterface $nft 
     * @return Response
     * @throws ApiCallException 
     */
    public function uploadNft(CardanoNftInterface $nft, MetadataUpload $metadata): Response
    {

        $existsRemotely = $this->checkIfExistsOnApi($nft);

        if ($existsRemotely) {
            throw new NftAlreadyExistException('The NFT already exists.', 409);
        }

        if (!$nft->maker_project_uuid) {
            throw new AttributeNotFoundException("Attribute 'nfts_project_uuid' not found", 409);
        };

        $ApiSchema = loadSchema('upload-nft');;
        $validMetadata = $this->validateMetadata($metadata->toArray(), $ApiSchema);

        if ($validMetadata) {
            $response = $this->callApi("/v2/UploadNft/{$nft->maker_project_uuid}", Method::POST, $metadata->toArray());
            return $response;
        }
    }

    /**
     * Updates an existing NFT on the Cardano NFT Maker API.
     *
     * @param CardanoNftInterface $nft 
     * @param array $metadata 
     * @return \Saloon\Http\Response 
     * @throws AttributeNotFoundException 
     * @throws ApiCallException 
     */
    public function updateNft(CardanoNftInterface $nft, $metadata): Response
    {

        if (!$nft->maker_project_uuid) {
            throw new AttributeNotFoundException("Attribute 'maker_project_uuid' not found", 409);
        };

        if (!$nft->maker_nft_uuid) {
            throw new AttributeNotFoundException("Attribute 'maker_nft_uuid' not found", 409);
        };

        if (!is_array($metadata)) {
            $type = gettype($metadata);
            throw new TypeMismatchException("metadata", 'array', $type, "expected array got {$type}");
        }

        $response = $this->callApi("/v2/UpdateMetadata/{$nft->maker_project_uuid}/{$nft->maker_nft_uuid}", Method::POST, $metadata);
        return $response;
    }

    /**
     * Deletes an existing NFT from the Cardano NFT Maker API.
     *
     * @param CardanoNftInterface $nft 
     * @return \Saloon\Http\Response 
     * @throws AttributeNotFoundException 
     * @throws ApiCallException 
     */
    public function deleteNft(CardanoNftInterface $nft): Response
    {
        if (!$nft->maker_nft_uuid) {
            throw new AttributeNotFoundException("Attribute 'maker_nft_uuid' not found", 409);
        };

        $response = $this->callApi("/v2/DeleteNft/{$nft->maker_nft_uuid}", Method::GET);
        return $response;
    }

    /**
     * Validates the NFT metadata against a given schema.
     *
     * @param array $metadata 
     * @param array $schema 
     * @return bool 
     * @throws TypeMismatchException 
     */
    function validateMetadata(array $metadata, array $schema): bool
    {
        foreach ($schema as $key => $type) {
            // dd($key);
            if (!array_key_exists($key, $metadata) && in_array($key, ['tokenname', 'displayname'])) {
                throw new TypeMismatchException($key, $type, 'missing', "Missing required field '$key'.");
            }

            if (is_array($type) && isset($metadata[$key])) {
                if ($key === 'subfiles' || $key === 'metadataPlaceholder') {
                    if (!is_array($metadata[$key])) {
                        throw new TypeMismatchException($key, 'array', gettype($metadata[$key]));
                    }

                    foreach ($metadata[$key] as $index => $item) {
                        if ($item) {
                            $this->validateMetadata($item, $type[0], "$key[$index]");
                        }
                    }
                } else {
                    if (!is_array($metadata[$key]) || !$this->validateMetadata($metadata[$key], $type, $key)) {
                        throw new TypeMismatchException($key, 'array', gettype($metadata[$key]));
                    }
                }
            } else {
                if (isset($metadata[$key]) && gettype($metadata[$key]) !== $type) {
                    throw new TypeMismatchException($key, $type, gettype($metadata[$key]));
                }
            }
        }

        return true;
    }

    /**
     * Checks if an NFT exists on the Cardano NFT Maker API.
     *
     * @param CardanoNftInterface $nft 
     * @return bool 
     * @throws Throwable 
     */
    public function checkIfExistsOnApi(CardanoNftInterface $nft): bool
    {
        if (!$nft->nft_uuid) {
            return false;
        }

        try {
            $response = $this->getMetadata($nft);

            if (isset($response['resultState']) && $response['resultState'] === 'Error' && $response['errorCode'] === 404) {
                return false;
            } else {
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }

    /**
     * Fetches an NFT from the Cardano NFT Maker API.
     *
     * @param CardanoNftInterface $nft 
     * @return Response 
     * @throws AttributeNotFoundException 
     * @throws ApiCallException 
     */
    public function getMetadata(CardanoNftInterface $nft): Response
    {
        if (!$nft->maker_nft_uuid) {
            throw new AttributeNotFoundException("Attribute 'maker_nft_uuid' not found", 409);
        }

        $response = $this->callApi("/v2/GetNftDetailsById/{$nft->maker_nft_uuid}", Method::GET);
        return $response;
    }

    /**
     * Makes an API call to the specified endpoint using the NftMakerRequest class.
     *
     * @param string $endpoint 
     * @param string $method 
     * @param mixed $body Optional.
     * @return \Saloon\Http\Response 
     * @throws ApiCallException 
     */
    public function callApi(string $endpoint, ?Method $method = Method::GET, mixed $body = null): Response
    {
        try {
            $makerReq = new NftMakerRequest($method);
            $makerReq->setEndPoint($endpoint);

            if ($body) {
                $makerReq->body()->set($body);
            }

            $response = $makerReq->send();

            if ($response->successful()) {
                return $response;
            } else {
                throw new ApiCallException('API call failed', $response->status(), null, $response);
            }
        } catch (Throwable $th) {
            Log::error('API call failed', [
                'endpoint' => $endpoint,
                'method' => $method,
                'exception' => $th
            ]);

            throw new ApiCallException('An error occurred during the API call', 0, $th);
        }
    }


}
