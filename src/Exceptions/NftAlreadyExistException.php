<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Exceptions;

use Exception;
use Throwable;

class NftAlreadyExistException extends Exception
{
    protected mixed $response;

    public function __construct($message = "The NFT already exists in remote API.", $code = 0, Throwable $previous = null, $response = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    /**
     * Returns the response from the remote API.
     *
     * @return mixed The response from the remote API.
     */
    public function getResponse()
    {
        return $this->response;
    }
}
