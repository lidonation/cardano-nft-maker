<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Exceptions;

use Exception;
use Throwable;

class ApiCallException extends Exception
{
    protected mixed $response;

    public function __construct($message = "", $code = 0, Throwable $previous = null, $response = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
