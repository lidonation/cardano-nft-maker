<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Exceptions;

use Exception;

class TypeMismatchException extends Exception
{
    protected string $field;
    protected string $expectedType;
    protected string $actualType;

    public function __construct($field, $expectedType, $actualType, $message = "", $code = 0, Exception $previous = null)
    {
        $message = $message ?: "Type mismatch for field '$field'in metadata: expected '$expectedType', got '$actualType'.";
        parent::__construct($message, $code, $previous);
        $this->field = $field;
        $this->expectedType = $expectedType;
        $this->actualType = $actualType;
    }

    /**
     * Retrieves the name of the field that caused the exception.
     *
     * @return string The name of the field.
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Retrieves the expected type of the field that caused the exception.
     *
     * @return string The expected data type of the field.
     */
    public function getExpectedType()
    {
        return $this->expectedType;
    }

    /**
     * Retrieves the actual type of the field that caused the exception.
     *
     * @return string The actual data type of the field.
     */
    public function getActualType()
    {
        return $this->actualType;
    }
}
