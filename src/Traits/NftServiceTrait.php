<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Traits;

use Saloon\Http\Response;
use Lidonation\CardanoNftMaker\DTO\MetadataUpload;
use Lidonation\CardanoNftMaker\Services\NftService;
use Lidonation\CardanoNftMaker\Services\NftMakerService;

trait NftServiceTrait
{
    /**
     * Retrieves an instance of the NftMakerService.
     *
     * @return NftMakerService 
     */
    final protected function getNftService(): NftMakerService
    {
        return new NftMakerService;
    }

    /**
     * Retrieves the metadata associated with the NFT.
     *
     * @return Response The response from the NFT service containing the metadata.
     *
     * @throws \Exception If there is an error during the retrieval process.
     */
    final public function getNMKRNftMetadata(): Response
    {
        return $this->getNftService()->getMetadata($this);
    }


    /**
     * Uploads an NFT with the specified metadata.
     *
     * @param mixed $metadata The metadata to be associated with the NFT.
     * @return Response The response from the NFT service after uploading the NFT.
     *
     * @throws \Exception If there is an error during the upload process.
     */
    final public function uploadNMKRNft(MetadataUpload $metadata): Response
    {
        return $this->getNftService()->uploadNft($this, $metadata);
    }

    /**
     * Updates an NFT with the specified metadata.
     *
     * @param mixed $metadata The metadata to be associated with the NFT.
     * @return Response The response from the NFT service after updating the NFT.
     *
     * @throws \Exception If there is an error during the update process.
     */
    final public function updateNMKRNft($metadata): Response
    {
        return $this->getNftService()->updateNft($this, $metadata);
    }

    /**
     * Deletes the NFT associated with the current object.
     *
     * @return Response The response from the NFT service after deleting the NFT.
     *
     * @throws \Exception If there is an error during the deletion process.
     */
    final public function deleteNMKRNft(): Response
    {
        return $this->getNftService()->deleteNft($this);
    }
}
