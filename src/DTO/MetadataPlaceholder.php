<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\DTO;


use Spatie\LaravelData\Data;

class MetadataPlaceholder extends Data
{
    public function __construct(
        public string $name,
        public string $value
    ) {
    }
}
