<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\DTO;


use Spatie\LaravelData\Data;

class Subfile extends Data
{
    public function __construct(
        public ?string $mimetype,
        public ?string $fileFromBase64,
        public ?string $fileFromsUrl,
        public ?string $fileFromIPFS,
    ) {
    }
}
