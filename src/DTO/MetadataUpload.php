<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\DTO;

use Spatie\LaravelData\Data;
use Lidonation\CardanoNftMaker\DTO\Subfile;

class MetadataUpload extends Data
{
    public function __construct(
        public string $tokenname,
        public string $displayname,
        public ?string $description,
        public ?Subfile $previewImageNft,
        /** @var SubfileContainer[] */
        public ?array $subfiles,
        /** @var MetadataPlaceholder[] */
        public ?array $metadataPlaceholder,
        public ?string $metadataOverride,
        public ?string $metadataOverrideCip68,
        public ?int $priceInLovelace,
        public ?bool $isBlocked,
    ) {
    }
}
