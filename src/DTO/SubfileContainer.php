<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\DTO;

use Spatie\LaravelData\Data;
use Lidonation\CardanoNftMaker\DTO\Subfile;

class SubfileContainer extends Data
{
    public function __construct(
        public ?Subfile $subfile,
        public ?string $description,
        /** @var MetadataPlaceholder[] */
        public ?array $metadataPlaceholder,
    ) {
    }
}
