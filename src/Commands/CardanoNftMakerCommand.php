<?php

namespace Lidonation\CardanoNftMaker\Commands;

use Illuminate\Console\Command;

class CardanoNftMakerCommand extends Command
{
    public $signature = 'cardano-nft-maker';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
