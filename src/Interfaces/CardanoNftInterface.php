<?php

declare(strict_types=1);

namespace Lidonation\CardanoNftMaker\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

/**
 * @property Attribute|string|null $maker_nft_uuid
 * @property Attribute|string|null $maker_project_uuid
 */
interface CardanoNftInterface 
{

}
