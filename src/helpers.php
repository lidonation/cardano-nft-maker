<?php

declare(strict_types=1);

if (!function_exists('loadSchema')) {

    
    /**
     * Loads and decodes a JSON schema from a file.
     *
     * This function reads a JSON schema file from the 'schemas' directory located in 'src' directory .
     *
     * @param string $schemaFile The name of the schema file to load. This should include the file extension.
     * @return array The decoded JSON schema as an associative array.
     * @throws Exception If the schema file does not exist.
     */
    function loadSchema(string $schemaFile): array
    {
        $filePath = __DIR__."/schemas/{$schemaFile}.json";
        
        if (!file_exists($filePath)) {
            throw new Exception("Schema file not found: " . $schemaFile);
        }

        $schemaJson = file_get_contents($filePath);
        return json_decode($schemaJson, true);
    }
}
